package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.SeleniumWrappers;

public class LoginPage {

	public WebDriver driver;
	public SeleniumWrappers wrapper;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		wrapper = new SeleniumWrappers(driver);
	}
	
	public By usernameField = By.id("log");
	public By passwordField = By.id("password");
	public By submitButton = By.cssSelector("input.submit_button");
	private By loginSuccessfulPopup = By.xpath("//div[contains(@class, 'sc_infobox') and contains(text(), 'Login success!')]");
	
	public void setUserName(String username) {
		driver.findElement(usernameField).sendKeys(username);
		
	}
	
	public void setPasswword(String password) {
		driver.findElement(passwordField).sendKeys(password);
		
	}
	
	public void clickSubmitButton() {
		driver.findElement(submitButton).click();
	}
	
	public void loginInApp(String username, String password) {
		driver.findElement(usernameField).sendKeys(username);
		driver.findElement(passwordField).sendKeys(password);
		driver.findElement(submitButton).click();
		wrapper.waitForElementToBeDisplayed(loginSuccessfulPopup);
		wrapper.waitForElementToBeDisappear(loginSuccessfulPopup);
	}
}
