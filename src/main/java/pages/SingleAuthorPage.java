package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.SeleniumWrappers;

public class SingleAuthorPage {

	WebDriver driver;
	SeleniumWrappers wrapper;
	
	public SingleAuthorPage(WebDriver driver) {
		this.driver = driver;
		wrapper = new SeleniumWrappers(driver);
	}
	
	private By dramaSkill = By.xpath("//div[contains(@class, 'sc_skills_item') and preceding::div[contains(text(), 'Drama')]][1]");
	
	private By biographySkill = By.xpath("//div[contains(@class, 'sc_skills_item') and preceding::div[contains(text(), 'Biography')]][1]");
	
	private By cookbookSkill = By.xpath("//div[contains(@class, 'sc_skills_item') and preceding::div[contains(text(), 'Cookbooks')]][1]");

	public String getDramaSkillValue() {
		wrapper.waitForSkillProgressBarToLoad(dramaSkill);
		return driver.findElement(dramaSkill).getText();
	}
	
	public String getBiographySkillValue() {
		wrapper.waitForSkillProgressBarToLoad(biographySkill);
		return driver.findElement(biographySkill).getText();
	}
	
	public String getCookBookSkillValue() {
		wrapper.waitForSkillProgressBarToLoad(cookbookSkill);
		return driver.findElement(cookbookSkill).getText();
	}

}
