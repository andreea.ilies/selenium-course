package pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import utils.SeleniumWrappers;

public class ShippingPage {
	
	WebDriver driver;
	WebElement dropdown;
	Select selectDropdown;
	SeleniumWrappers wrapper;
	
	public ShippingPage(WebDriver driver) {
		this.driver = driver;
		wrapper = new SeleniumWrappers(driver);
	}
	
	private By countryDropDown = By.name("shipping_country");
	
	private By countryDropDownLabel = By.xpath("//label[@for='shipping_country']");
	
	private By provinceDropDown = By.name("shipping_state");
	
	private By provinceDropDownLabel = By.xpath("//label[@for='shipping_state']");
	
	public void countryFilterByIndex(String countryName) {
		wrapper.waitForElementToBeDisplayed(countryDropDown);
		wrapper.waitTextToBePresentInElement(countryDropDownLabel, "Country");
		dropdown = driver.findElement(countryDropDown);
		selectDropdown = new Select(dropdown);
		List<WebElement> countries = selectDropdown.getOptions();
		int index = 0;
		for(WebElement country : countries) {
			if(country.getText().equals(countryName)) {
				index = countries.indexOf(country);
				break;
			}
		}
		selectDropdown.selectByIndex(index);
	}
	

	public void provinceFilterByValue(String provinceName) {
		wrapper.waitForElementToBeDisplayed(provinceDropDown);
		wrapper.waitTextToBePresentInElement(provinceDropDownLabel, "Province");
		dropdown = driver.findElement(provinceDropDown);
		selectDropdown = new Select(dropdown);
		List<WebElement> provinces = selectDropdown.getOptions();
		String value = "";
		for(WebElement province : provinces) {
			if(province.getText().equals(provinceName)) {
				value = province.getAttribute("value");
			}
		}
		selectDropdown.selectByValue(value);
	}

	public String getCountrySelection() {
		dropdown = driver.findElement(countryDropDown);
		selectDropdown = new Select(dropdown);
		return selectDropdown.getFirstSelectedOption().getText();
	}
	
	public String getProvinceSelection() {
		dropdown = driver.findElement(provinceDropDown);
		selectDropdown = new Select(dropdown);
		return selectDropdown.getFirstSelectedOption().getText();
	}
	
}
