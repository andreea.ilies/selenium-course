package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class NavMenuPage {

	public WebDriver driver;
	
	public NavMenuPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public By loginLink = By.linkText("Login");
	
	public By shopLink = By.linkText("BOOKS");
	
	public By singleAuthorLink = By.linkText("SINGLE AUTHOR");
	
	public By shippingLink = By.linkText("SHIPPING");
	
	public LoginPage navToLogin() {
		driver.findElement(loginLink).click();
		return new LoginPage(driver);
	}
	
	public ShopPage navToShopPage() {
		driver.findElement(shopLink).click();
		return new ShopPage(driver);
	}
	
	public SingleAuthorPage navToSingleAuthorPage() {
		driver.findElement(singleAuthorLink).click();
		return new SingleAuthorPage(driver);
	}
	
	public ShippingPage navToShippingPage() {
		driver.findElement(shippingLink).click();
		return new ShippingPage(driver);
	}
}
