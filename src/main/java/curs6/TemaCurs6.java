package curs6;

import static org.testng.Assert.assertTrue;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import utils.BaseTest;

public class TemaCurs6 extends BaseTest{
	
	@Parameters({"name", "email", "subject", "message"})
	@Test
	public void buyBook(String name, String email, String subject, String message) throws Exception {
		
		WebElement inputName = driver.findElement(By.name("your-name"));
		WebElement inputEmail = driver.findElement(By.name("your-email"));
		WebElement inputSubject = driver.findElement(By.name("your-s"));
		WebElement inputMessage = driver.findElement(By.name("your-message"));
		WebElement buttonSendMessage = driver.findElement(By.xpath("//input[@value='Send Message']"));
		
		inputName.sendKeys(name);
		inputEmail.sendKeys(email);
		inputSubject.sendKeys(subject);
		inputMessage.sendKeys(message);
		buttonSendMessage.click();
		Thread.sleep(2000);
		
		WebElement messageSent = driver.findElement(By.xpath("//div[text()='Thank you for your message. It has been sent.']"));
		assertTrue(messageSent.isDisplayed());
	}
	
}
