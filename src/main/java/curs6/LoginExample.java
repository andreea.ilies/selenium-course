package curs6;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import utils.BaseTest;

public class LoginExample extends BaseTest{

	@Parameters({"user", "pass"})
	@Test
	public void validLogin(String user, String pass) throws InterruptedException {
		WebElement loginMenu = driver.findElement(By.xpath("//li[@class='menu_user_login']"));
		loginMenu.click();
		Thread.sleep(2000);
		
		WebElement userNameField = driver.findElement(By.xpath("//input[@id='log' or @name='log']"));
		userNameField.sendKeys(user);
		WebElement passwordField = driver.findElement(By.xpath("//input[@id='pwd' or @name='pwd']"));
		passwordField.sendKeys(pass);
	}
	
	@Test
	public void invalidLogin() {
		
	}
	
}
