package tests;

import static org.testng.Assert.assertEquals;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.ShippingPage;
import utils.BaseTest;

public class ShippingTest extends BaseTest{

	@Parameters({"user", "pass"})
	@Test
	public void shippingTest(String user, String pass) {
		
		LoginPage loginPage = navMenu.navToLogin();
		loginPage.loginInApp(user, pass);
		
		driver.get("https://keybooks.ro/account/edit-address/shipping/");
		
		ShippingPage shipping = new ShippingPage(driver);
		shipping.countryFilterByIndex("Canada");
		shipping.provinceFilterByValue("Newfoundland and Labrador");
		
		assertEquals(shipping.getCountrySelection(), "Canada");
		assertEquals(shipping.getProvinceSelection(), "Newfoundland and Labrador");
	}
}
