package tests;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import pages.ShopPage;
import utils.BaseTest;

public class DropdownTest extends BaseTest {

	@Test
	public void dropdownFilterByValueExample() {
		
		ShopPage shopPage = navMenu.navToShopPage();
		shopPage.filterByValue("popularity");
		assertEquals(shopPage.getTheCurrentSelectedOption(), "Sort by rating");
	}
	
	@Test
	public void drpodownFilterByIndexExample() {
		ShopPage shopPage = navMenu.navToShopPage();
		shopPage.filterByIndex(3);
		assertEquals(shopPage.getTheCurrentSelectedOption(), "Sort by latest");
	}
	
	@Test
	public void dropDownFilterByVisibleTextExample() {
		ShopPage shopPage = navMenu.navToShopPage();
		shopPage.filterByVisibleText("Sort by price: high to low");
		assertEquals(shopPage.getTheCurrentSelectedOption(), "Sort by price: high to low");
	}
}
