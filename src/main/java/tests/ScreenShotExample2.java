package tests;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import pages.ShopPage;
import utils.BaseTest;
import utils.TestNgListener;

@Listeners(TestNgListener.class)
public class ScreenShotExample2 extends BaseTest{

	@Test
	public void screenShotExample2Pass() {
		ShopPage shopPage = navMenu.navToShopPage();
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/shop/");
	}
	
	@Test
	public void screenShotExample2Fail() {
		ShopPage shopPage = navMenu.navToShopPage();
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/shop2/");
	}
	
	@Test(dependsOnMethods = {"screenShotExample2Fail"})
	public void screenShotExample2Skip() {
		ShopPage shopPage = navMenu.navToShopPage();
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/shop2/");
	}
}
