package tests;

import java.io.IOException;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.LoginPage;
import utils.BaseTest;

public class LoginTest extends BaseTest{

	@Parameters({"user", "pass"})
	@Test
	public void loginTest(String user, String pass) throws InterruptedException, IOException {
		
		LoginPage loginPage = navMenu.navToLogin();
		loginPage.loginInApp(user, pass);
		//loginPage.setUserName(user);
		//loginPage.setPasswword(pass);
		//loginPage.clickSubmitButton();
	}
}
