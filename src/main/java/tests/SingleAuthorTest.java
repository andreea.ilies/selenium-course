package tests;

import static org.testng.Assert.assertEquals;
import org.testng.annotations.Test;
import pages.SingleAuthorPage;
import utils.BaseTest;

public class SingleAuthorTest extends BaseTest{

	@Test
	public void singleAuthorSkillsTest() {
		
		SingleAuthorPage author = navMenu.navToSingleAuthorPage();
		
		assertEquals(author.getDramaSkillValue(), "95%");
		assertEquals(author.getBiographySkillValue(), "75%");
		assertEquals(author.getCookBookSkillValue(), "82%");
	}
}
