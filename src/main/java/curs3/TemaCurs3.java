package curs3;

import static org.testng.Assert.assertTrue;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TemaCurs3 {

	WebDriver driver;
	
	@BeforeClass
	public void setUp() {
		
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver3.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://keybooks.ro");
		
	}
	
	@Test
	public void checkBook() throws InterruptedException {

		List<WebElement> categories = driver.findElements(By.cssSelector("li[class*='sc_tabs_title']"));
		WebElement theForest = null;
		for(WebElement category: categories) {
			int count = 0;
			category.click();
			Thread.sleep(2000);
			List<WebElement> displayedBooksTitles = driver.findElements(By.xpath("//h3[contains(@class, 'sc_title') and ancestor::div[@aria-hidden='false']]"));
			for(WebElement displayedBookTitle: displayedBooksTitles) {
				if(displayedBookTitle.getText().equals("The forest")) {
					count++;
					theForest = displayedBookTitle;
				}
			}
			assertTrue(count == 1);	
		}
		theForest.click();
		Thread.sleep(2000);
		assertTrue(driver.getCurrentUrl().equals("https://keybooks.ro/shop/the-forest/"));
			
	}
		
	
	@AfterClass
	public void tearDown() {
		driver.quit();
	}
	
}
