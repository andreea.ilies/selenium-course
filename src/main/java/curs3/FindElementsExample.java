package curs3;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import utils.BaseTest;

public class FindElementsExample extends BaseTest{

	@Test
	public void testFindElements(){
		//li[class*='sc_tabs_title']
		List<WebElement> books = driver.findElements(By.cssSelector("figure[class*='sc_title']"));
		books.get(2).click();
		String currentUrl = driver.getCurrentUrl();
		String expectedUrl = "https://keyboards.ro/shop/life-in-the-garden/";
		assertEquals(currentUrl, expectedUrl);
		
		String currentTitle = driver.getTitle();
		String expectedTitle = "Life in the garden � Booklovers";
		assertEquals(currentTitle, expectedTitle);
	}
	
	@Test
	public void testFindElements2(){

		List<WebElement> images = driver.findElements(By.cssSelector("figure[class*='sc_image']"));
		for(WebElement image: images) {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].setAttribute('style', 'backgroud:yellow; border:2px solid red;')", image);
		}
	}
	
}
