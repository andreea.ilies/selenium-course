package curs4;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import utils.BaseTest;

public class CssSelectors extends BaseTest{

	@Test
	public void testCssSelectors() {
		
		//id --> #
		WebElement loginPopUp = driver.findElement(By.cssSelector("#menu_user"));
		
		//class --> .
		WebElement logoSlogan = driver.findElement(By.cssSelector(".logo_slogan"));
		
		//[]
		//atribut
		WebElement element = driver.findElement(By.cssSelector("h3[style='text-align:center']"));
		//subclasa
		WebElement element2 = driver.findElement(By.cssSelector("div.column-1_2 h3.sc_title_underline"));
		WebElement element3 = driver.findElement(By.cssSelector("div[class*=column-1_2]"));
		
		//or
		
		
	}
}
