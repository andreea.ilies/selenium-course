package curs4;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TemaCurs4 {

	WebDriver driver;
	
	@BeforeClass
	public void setUp() {
		
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver3.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://keybooks.ro");
		
	}
	
	@Test
	public void checkBook() throws Exception {

		WebElement bookLifeInTheGarden = driver.findElement(By.cssSelector("a[href='life-in-the-garden']"));
		bookLifeInTheGarden.click();
		Thread.sleep(2000);
		
		WebElement reviewTab = driver.findElement(By.cssSelector("li.reviews_tab"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", reviewTab);
		reviewTab.click();
		Thread.sleep(2000);
		
		WebElement checkBox = driver.findElement(By.cssSelector("input[name='wp-comment-cookies-consent']"));
		assertFalse(checkBox.isSelected());
		
		WebElement review = driver.findElement(By.cssSelector("textarea#comment"));
		WebElement name = driver.findElement(By.cssSelector("input#author"));
		WebElement email = driver.findElement(By.cssSelector("input#email"));
		WebElement submitButton = driver.findElement(By.cssSelector("input#submit"));
		setRating(3);
		review.sendKeys("Best book");
		name.sendKeys("Andreea Mihai");
		email.sendKeys("andreea53171@gmail.com");
		checkBox.click();
		assertTrue(checkBox.isSelected());
		submitButton.click();
		Thread.sleep(2000);
			
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", reviewTab);
		WebElement reviewApproval = driver.findElement(By.cssSelector(".woocommerce-review__awaiting-approval"));
		assertTrue(reviewApproval.isDisplayed());
	}
	
	private void setRating(int stars) throws Exception {
		if(stars < 1 || stars > 5) {
			throw new Exception("Rating invalid!");
		}
		WebElement rating = driver.findElement(By.cssSelector("p.stars>span>a:nth-child(" + stars + ")"));
		Thread.sleep(2000);
		rating.click();
		Thread.sleep(2000);
	}
		
	
	@AfterClass
	public void tearDown() throws InterruptedException {
		Thread.sleep(4000);
		driver.quit();
	}
	
}
