package curs5;

import static org.testng.Assert.assertTrue;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TemaCurs5 {

	WebDriver driver;
	
	@BeforeClass
	public void setUp() {
		
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver3.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://keybooks.ro");
		
	}
	
	@Test
	public void buyBook() throws Exception {

		String bookName = "The story about me";
		WebElement searchButton = driver.findElement(By.cssSelector("button.search_submit[title='Open search']"));
		WebElement searchBar = driver.findElement(By.cssSelector("button.search_submit[title='Open search']:first-child"));
		
		searchButton.click();
		searchBar.sendKeys(bookName);
		searchBar.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		
		List<WebElement> displayedBooksElements = driver.findElements(By.xpath("//a[parent::h4[@class='post_title']]"));
		List<String> displayedBooksNames = new ArrayList<>();
		displayedBooksElements.forEach(
	            (temp) -> { displayedBooksNames.add(temp.getText());});

		WebElement buttonLoadMore = driver.findElement(By.cssSelector("a.viewmore_button"));
		while(!displayedBooksNames.contains(bookName)) {
			buttonLoadMore.click();
			Thread.sleep(2000);
			displayedBooksElements.clear();
			displayedBooksNames.clear();
			displayedBooksElements = driver.findElements(By.xpath("//a[parent::h4[@class='post_title']]"));
			displayedBooksElements.forEach(
		            (temp) -> { displayedBooksNames.add(temp.getText());});
		}
		
		WebElement searchedBook = driver.findElement(By.xpath("//h4[contains(@class, 'post_title') and descendant::a[text()='" + bookName + "']]"));
		searchedBook.click();
		Thread.sleep(2000);
		
		assertTrue(driver.getCurrentUrl().equals("https://keybooks.ro/shop/the-story-about-me/"));
		
		WebElement buttonAddToCart = driver.findElement(By.cssSelector("button.single_add_to_cart_button"));
		buttonAddToCart.click();
		Thread.sleep(2000);
		
		WebElement messageItemAdedToCart = driver.findElement(By.xpath("//div[text()=' �" + bookName + "� has been added to your cart.	']"));
		assertTrue(messageItemAdedToCart.isDisplayed());
		
		WebElement buttonViewCart = driver.findElement(By.xpath("//a[contains(@class, 'wc-forward') and text()='View cart' and parent::div[contains(@class, 'woocommerce-message')]]"));
		buttonViewCart.click();
		Thread.sleep(2000);
		
		WebElement inputQuantity = driver.findElement(By.xpath("//input[contains(@class, 'qty') and preceding-sibling::label[contains(text(),'" + bookName + "')]]"));
		inputQuantity.clear();
		inputQuantity.sendKeys("2");
		inputQuantity.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		WebElement buttonUpdateCart = driver.findElement(By.cssSelector("button[name='update_cart']"));
		buttonUpdateCart.click();
		Thread.sleep(2000);
		
		WebElement messageCartUpdated = driver.findElement(By.xpath("//div[contains(text(),'Cart updated.')]"));
		assertTrue(messageCartUpdated.isDisplayed());
		
		WebElement buttonProceedToCheckout = driver.findElement(By.cssSelector("a.checkout-button"));
		buttonProceedToCheckout.click();
		Thread.sleep(2000);
		
		assertTrue(driver.getCurrentUrl().equals("https://keybooks.ro/checkout/"));
		WebElement textBillingDetails = driver.findElement(By.xpath("//h3[contains(text(), 'Billing details')]"));
		WebElement textAdditionalInformation = driver.findElement(By.xpath("//h3[contains(text(), 'Additional information')]"));
		assertTrue(textBillingDetails.isDisplayed());
		assertTrue(textAdditionalInformation.isDisplayed());
		
	}
	
	@AfterClass
	public void tearDown() throws InterruptedException {
		Thread.sleep(4000);
		driver.quit();
	}
	
}
