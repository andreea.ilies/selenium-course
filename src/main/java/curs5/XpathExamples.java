package curs5;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import utils.BaseTest;

public class XpathExamples extends BaseTest{
	
	@Test(priority=0)
	public void testXpathExample(){
		
		WebElement loginMenu = driver.findElement(By.xpath("//li[@class='menu_user_login']"));
		loginMenu.click();
		
		WebElement userNameField = driver.findElement(By.xpath("//input[@id='log' or @name='log']"));
		userNameField.sendKeys("TestUser");
		WebElement passwordField = driver.findElement(By.xpath("//input[@id='pwd' or @name='pwd']"));
		passwordField.sendKeys("12345@67890");
		WebElement rememberMe = driver.findElement(By.xpath("//input[@id='rememberme']"));
		rememberMe.click();
		WebElement loginButton = driver.findElement(By.xpath("//input[@value='Login']"));
		loginButton.click();
		
	}
	
	@Test(priority=1, dependsOnMethods = "testXpathExample")
	public void testAccountDetails() {
		navigateToAccount();
		WebElement recentOrdersLink = driver.findElement(By.xpath("//a[contains(text(), 'recent orders')]"));
		recentOrdersLink.click();
		
	}

}
