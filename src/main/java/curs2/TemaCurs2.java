package curs2;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TemaCurs2 {

	WebDriver driver;
	
	@BeforeClass
	public void setUp() {
		
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver3.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://keybooks.ro");
		
	}
	
	@Test
	public void checkLogin() {

		WebElement login = driver.findElement(By.className("menu_user_login"));
		WebElement email = driver.findElement(By.id("log"));
		WebElement password = driver.findElement(By.id("password"));
		
		assertTrue(login.isDisplayed(), "Login button is displayed");
		assertTrue(!email.isDisplayed(), "Email input is NOT displayed");
		assertTrue(!password.isDisplayed(), "Password input is NOT displayed");
		
		login.click();
		assertTrue(email.isDisplayed(), "Email input is displayed");
		assertTrue(password.isDisplayed(), "Password input is displayed");
	}
	
	@AfterClass
	public void tearDown() {
		driver.quit();
	}
	
}
