package curs2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import okhttp3.WebSocket;

public class FirstSeleniumScript {

	WebDriver driver;
	
	@BeforeClass
	public void setUp() {
		
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver3.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.keybooks.ro/");
		
	}
	
	@Test
	public void test1() {
		String webPageTitie = driver.getTitle();
		System.out.println(webPageTitie);
		
		String sloganText = driver.findElement(By.className("logo_slogan")).getText();
		System.out.println(sloganText);
	}
	
	@AfterClass
	public void tearDown() {
		driver.quit();
	}
}
