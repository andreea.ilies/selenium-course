package curs2;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WebElementExample {

WebDriver driver;
	
	@BeforeClass
	public void setUp() {
		
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver3.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://keybooks.ro");
		
	}
	
	//@Test
	public void test1() {

		WebElement email = driver.findElement(By.id("email"));
		if(email.isEnabled()) {
			email.click();
			email.sendKeys("email@ceva.com");
		}
	}
	
	@Test(priority=0)
	public void linkTextLocatorExample() {
		WebElement booksLink = driver.findElement(By.linkText("BOOKS"));
		booksLink.click();
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/shop/");
	}
	
	@Test(priority=1)
	public void partialLinkTextLocatorExample() {
		WebElement cookingBook = driver.findElement(By.partialLinkText("Cooking"));
		cookingBook.click();
		assertEquals(driver.getCurrentUrl(), "https://keybooks.ro/shop/cooking-with-love/");
	}
	
	@Test(priority = 3)
	public void classNameExample() {
		WebElement productTitle = driver.findElement(By.className("product_title entry-title"));
		productTitle.isDisplayed();
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].setAttribute('style', 'background:yellow; border: 2px solid red')", productTitle);
	}
	
	@AfterClass
	public void tearDown() {
		driver.quit();
	}
	
}
