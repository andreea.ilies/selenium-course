package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumWrappers {

	WebDriver driver;
	
	public SeleniumWrappers (WebDriver driver) {
		this.driver = driver;
	}
	
	public void customClick(By element) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(element));
			WebElement webElement = driver.findElement(element);
			webElement.click();
		}catch(Exception e) {
			//retry mechanism
		}
	}
	
	public void waitForElementToBeDisplayed(By element) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.presenceOfElementLocated(element));
	}
	
	public void waitForElementToBeDisappear(By element) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(element));
	}
	
	public void waitForSkillProgressBarToLoad(By element) {
		waitTextToBePresentInElement(element, "%");
	}
	
	public void waitTextToBePresentInElement(By element, String text) {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.textToBePresentInElementLocated(element, text));
	}
	
	public void scrollToElement(By element) {
		WebElement webElement = driver.findElement(element);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
	}
	
}
