package utils;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.time.Duration;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.google.common.io.Files;

import pages.NavMenuPage;

public class BaseTest {

	public WebDriver driver;
	public NavMenuPage navMenu;
	
	@Parameters({"url"})
	@BeforeClass
	public void setUp(String url) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver3.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(url);

		navMenu = new NavMenuPage(driver);
	}
	
	@AfterClass
	public void tearDown() throws InterruptedException {
		Thread.sleep(4000);
		driver.quit();
	}
	
	@AfterMethod
	public void recordFailure(ITestResult result) {
		if(ITestResult.FAILURE == result.getStatus()) {
			TakesScreenshot poza = (TakesScreenshot) driver;
			File picture = poza.getScreenshotAs(OutputType.FILE);
			try {
				Files.copy(picture, new File("screenshots/" + result.getName() + ".png"));
			} catch(Exception e) {
				System.out.println("Could not take picture");
				e.printStackTrace();
			}
		}
	}
	
	public void navigateToAccount() {
		driver.get("https://keybooks.ro/account/");
	}
	
	public void waitExample() {
		WebElement element = driver.findElement(By.id("finish"));
		String text = "Hello World";
		
		//asteptam ca element sa fie vizibil
//		WebDriverWait wait = new WebDriverWait(driver, 10);
//		wait.until(ExpectedConditions.textToBePresentInElement(element, text));
		
		//v2
		FluentWait<WebDriver> waitV2 = new FluentWait<>(driver)
				.withTimeout(Duration.ofSeconds(6))
				.pollingEvery(Duration.ofMillis(200))
				.ignoring(NoSuchElementException.class);
		waitV2.until(ExpectedConditions.textToBePresentInElement(element, text));
		
		assertEquals(element.getText(), text);
	}
}
